package com.yng.oop.crawler.storage.export;

import com.yng.oop.crawler.consumer.joblisting.JobListing;

import java.io.IOException;
import java.util.List;

/**
 * Interface for exporting job listings.
 */
public interface IJobListingExporter {
    /**
     * @param jobListings List of job listings to be exported
     */
    void export(List<JobListing> jobListings);
}
