package com.yng.oop.crawler.storage.export;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

/**
 * An implementation of the IJobListingExporter that exports job listings to a CSV file.
 */
@AllArgsConstructor
public class CSVJobListingExporter implements IJobListingExporter {
    Logger logger = LogManager.getLogger(CSVJobListingExporter.class);
    OutputStream outputStream;
    String delimiter;

    /**
     * Constructs a CSVJobListingExporter with the specified output stream and default delimiter "," (coma)
     *
     * @param outputStream The output stream to write CSV data to.
     */
    public CSVJobListingExporter(OutputStream outputStream) {
        this.outputStream = outputStream;
        delimiter = "\t";
    }

    /**
     * Export job listings to csv
     *
     * @param jobListings List of job listings to be exported
     */
    @Override
    public void export(List<JobListing> jobListings) {

        PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));
        // Title of columns in csv file
        String columnTitles = String.join(delimiter,
                "URL",
                "Title",
                "SalaryMax",
                "SalaryMin",
                "Location",
                "Firm"
        ).concat("\r\n");

        try {
            outputStream.write(columnTitles.getBytes());

            // Actual data in csv format
            for (JobListing jl : jobListings) {
                String tmpJobListing = String.join(delimiter,
                        jl.getURL(),
                        jl.getTitle(),
                        jl.getSalaryMax(),
                        jl.getSalaryMin(),
                        jl.getLocation(),
                        jl.getFirm()
                ).concat("\r\n");
                outputStream.write(tmpJobListing.getBytes());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
