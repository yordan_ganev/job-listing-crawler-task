package com.yng.oop.crawler.storage.export;

import com.yng.oop.crawler.consumer.joblisting.JobListing;

import java.io.IOException;
import java.util.List;

/**
 * An implementation of the IJobListingExporter interface that exports job listings to the console.
 */
public class ConsoleJobListingExporter implements IJobListingExporter {
    /**
     * Exports the given list of job listings to the console.
     *
     * @param jobListings The list of job listings to be exported.
     */
    @Override
    public void export(List<JobListing> jobListings) {
        for (JobListing jobListing : jobListings) {
            System.out.println(jobListing);
        }
    }
}
