package com.yng.oop.crawler.storage;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class for in-memory storing job listings.
 */
public final class Storage {
    Logger logger = LogManager.getLogger(Storage.class);
    private static volatile Storage instance;
    private final List<JobListing> jobListings;

    /**
     * Private constructor of singleton used to init list of job listings.
     */
    private Storage() {
        jobListings = new ArrayList<>();
    }

    /**
     * Adds a job listing to the storage.
     *
     * @param jobListing The job listing to be added.
     */
    public synchronized void addJobListing(JobListing jobListing) {
        jobListings.add(jobListing);
        logger.trace("Storing: " + jobListing);
    }

    /**
     * @return A copy of the list of job listings.
     */
    public synchronized List<JobListing> getJobListings() {
        return new ArrayList<>(jobListings);
    }

    /**
     * @return The singleton instance of the Storage class.
     */
    public static Storage getInstance() {
        Storage result = instance;

        //double-checked locking
        if (result != null) {
            return result;
        }

        synchronized (Storage.class) {
            if (instance == null) {
                instance = new Storage();
            }
            return instance;
        }

    }
}