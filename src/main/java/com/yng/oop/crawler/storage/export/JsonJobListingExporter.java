package com.yng.oop.crawler.storage.export;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * An implementation of the IJobListingExporter that exports job listings to JSON format.
 */
public class JsonJobListingExporter implements IJobListingExporter {
    Logger logger = LogManager.getLogger(JsonJobListingExporter.class);
    OutputStream outputStream;

    /**
     * Constructs a JsonJobListingExporter with the specified output stream.
     *
     * @param outputStream The output stream to write JSON data to
     */
    public JsonJobListingExporter(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * Exports the given list of job listings to a JSON
     *
     * @param jobListings The list of job listings to be exported
     */
    @Override
    public void export(List<JobListing> jobListings) {
        JSONArray root = new JSONArray();

        for (JobListing jl : jobListings) {
            JSONObject jobListing = new JSONObject();
            jobListing.put("url", jl.getURL());
            jobListing.put("title", jl.getTitle());

            JSONObject options = getJsonOptions(jl);
            jobListing.put("options", options);

            logger.trace(jobListing.toString());
            root.put(jobListing);
        }
        logger.trace(root.toString());

        try {
            outputStream.write(root.toString().getBytes());
        } catch (IOException e) {
            logger.debug(e.getMessage());
            logger.debug(e.getStackTrace().toString());
        }
    }

    /**
     * Constructs a JSON object containing none, some or all job listing options (firm, location, salaryMin, salaryMax).
     *
     * @param jl The job listing for which to construct options.
     * @return A JSONObject containing job listing options.
     */
    private static JSONObject getJsonOptions(JobListing jl) {
        JSONObject options = new JSONObject();
        if (!jl.getFirm().isBlank()) {
            options.put("firm", jl.getFirm());
        }
        if (!jl.getLocation().isBlank()) {
            options.put("location", jl.getLocation());
        }
        if (!jl.getSalaryMin().isBlank()) {
            options.put("salaryMin", jl.getSalaryMin());
        }
        if (!jl.getSalaryMax().isBlank()) {
            options.put("salaryMax", jl.getSalaryMax());
        }
        return options;
    }
}

