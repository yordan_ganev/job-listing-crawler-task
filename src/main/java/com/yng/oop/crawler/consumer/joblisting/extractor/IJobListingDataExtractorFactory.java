package com.yng.oop.crawler.consumer.joblisting.extractor;

/**
 * Factory interface for creating instances of IJobListingDataExtractor.
 */
public interface IJobListingDataExtractorFactory {
    /**
     * Creates an instance of IJobListingDataExtractor based on the provided URL
     *
     * @param url The URL for data extractor
     * @return An instance of IJobListingDataExtractor suitable for the given URL, or null if no extractor is found
     */
    IJobListingDataExtractor createExtractor(String url);
}
