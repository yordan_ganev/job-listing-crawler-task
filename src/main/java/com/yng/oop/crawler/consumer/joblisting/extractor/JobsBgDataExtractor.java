package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import com.yng.oop.crawler.consumer.joblisting.JobListingBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Extractor implementation for job listings from jobs.bg
 */
public class JobsBgDataExtractor implements IJobListingDataExtractor {
    Logger logger = LogManager.getLogger(JobsBgDataExtractor.class);

    /**
     * @param document The HTML document to parse
     * @return The JobListing object containing extracted data, or null if extraction fails
     */
    @Override
    public JobListing parseDocument(Document document) {
        Elements tmpElements = document.select(".view-extra > .flex-1 .bold");
        String title = tmpElements.get(0).text();

        if (title == null || title.isBlank()) {
            return null;
        }
        JobListingBuilder builder = new JobListingBuilder(document.baseUri(), title);

        //Try to collect optional data
        String firm = "";
        String location = "";
        String salaryMin = "";
        String salaryMax = "";

        tmpElements = document.select("div.options i:contains(location) + span");

        if (!tmpElements.isEmpty()) {
            location = tmpElements.get(0).text();
        }

        tmpElements = document.select("div.options li > span > b");
        if (!tmpElements.isEmpty()) {
            String arr[] = tmpElements.get(0).text().split(" ");
            if (arr.length == 5) {
                String currency = arr[arr.length - 1];
                salaryMin = arr[1] + " " + currency;
                salaryMax = arr[3] + " " + currency;
            }
            if (arr.length == 2) {
                salaryMin = tmpElements.get(0).text();
            }
        }

        return builder.firm(firm)
                .location(location)
                .salaryMin(salaryMin)
                .salaryMax(salaryMax)
                .build();
    }
}
