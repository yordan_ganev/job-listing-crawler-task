package com.yng.oop.crawler.consumer.joblisting;

/**
 * Builder class for creating instances of JobListing
 */
public class JobListingBuilder {
    private String URL;
    private String title;
    private String salaryMax = "";
    private String salaryMin = "";
    private String location = "";
    private String firm = "";

    /**
     * Constructs a JobListingBuilder with the specified URL and title
     *
     * @param URL   The URL of the job listing
     * @param title The title of the job listing
     */
    public JobListingBuilder(String URL, String title) {
        this.URL = URL;
        this.title = title;
    }

    /**
     * @param salaryMax maximum salary for the job listing
     * @return The JobListingBuilder instance
     */
    public JobListingBuilder salaryMax(String salaryMax) {
        this.salaryMax = salaryMax;
        return this;
    }

    /**
     * @param salaryMin minimum salary for the job listing
     * @return The JobListingBuilder instance
     */
    public JobListingBuilder salaryMin(String salaryMin) {
        this.salaryMin = salaryMin;
        return this;
    }

    /**
     * @param location Location of the job listing
     * @return The JobListingBuilder instance
     */
    public JobListingBuilder location(String location) {
        this.location = location;
        return this;
    }

    /**
     * @param firm The firm or business offering the job listing
     * @return The JobListingBuilder instance
     */
    public JobListingBuilder firm(String firm) {
        this.firm = firm;
        return this;
    }

    /**
     * Builds and returns a new JobListing instance with the configured attributes
     *
     * @return The created JobListing instance
     */
    public JobListing build() {
        // Creating JobListing instance with all the provided fields
        JobListing jobListing = new JobListing(URL, title, salaryMax, salaryMin, location, firm);
        return jobListing;
    }
}
