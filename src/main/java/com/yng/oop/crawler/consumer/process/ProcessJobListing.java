package com.yng.oop.crawler.consumer.process;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import com.yng.oop.crawler.consumer.joblisting.extractor.IJobListingDataExtractor;
import com.yng.oop.crawler.consumer.joblisting.extractor.JobListingDataExtractorFacory;
import com.yng.oop.crawler.fetch.JsoupPageFetcher;
import com.yng.oop.crawler.fetch.PageFetch;
import com.yng.oop.crawler.storage.Storage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;

/**
 * Processing job listings.
 */
public class ProcessJobListing {
    Logger logger = LogManager.getLogger(ProcessJobListing.class);
    private final String url;
    private Document document;
    private JobListing jobListing;
    private ProcessState state;

    /**
     * States of the job listing processing
     */
    public enum ProcessState {
        GET_DOCUMENT,
        EXTRACT_DATA,
        SAVE_DATA,
        FINISHED
    }

    /**
     * Constructs a ProcessJobListing object with the specified URL
     *
     * @param url The URL of the job listing to process.
     */
    public ProcessJobListing(String url) {
        this.url = url;
        state = ProcessState.GET_DOCUMENT;
    }

    /**
     * Processes the job listing by state order
     */
    public void process() {
        if (!getDocument()) {
            return;
        }
        if (!extractDocumentData()) {
            return;
        }
        saveData();
    }

    /**
     * Fetches the document from the URL
     *
     * @return True - document is successfully fetched, false otherwise
     */
    private boolean getDocument() {
        if (state != ProcessState.GET_DOCUMENT) {
            logger.error("Invalid state! Expected: " + ProcessState.GET_DOCUMENT + "Got: " + state);
            return false;
        }

        PageFetch fetcher = new PageFetch(new JsoupPageFetcher());
        document = fetcher.fetch(url);
        if (document == null) {
            logger.error("Document is null (src: " + url + ")");
            return false;
        }
        logger.trace("Got document");
        state = ProcessState.EXTRACT_DATA;
        return true;
    }

    /**
     * Extracts data from the document
     *
     * @return True - data is successfully extracted, false otherwise
     */
    private boolean extractDocumentData() {
        if (state != ProcessState.EXTRACT_DATA) {
            logger.error("Invalid state! Expected: " + ProcessState.EXTRACT_DATA + "Got: " + state);
            return false;
        }

        IJobListingDataExtractor extractor = new JobListingDataExtractorFacory().createExtractor(url);
        if (extractor == null) {
            logger.error("Unknown URL " + url);
            logger.error("Could not create proper JobListingDataExtractor");
            return false;
        }

        jobListing = extractor.parseDocument(document);
        if (jobListing == null) {
            logger.error("Unable to parse document data (src: " + url + ")");
            return false;
        }

        logger.trace("Extracted " + jobListing + " from document");
        state = ProcessState.SAVE_DATA;
        return true;
    }

    /**
     * Saves the extracted data.
     *
     * @return True - successfully saved, false otherwise.
     */
    private boolean saveData() {
        if (state != ProcessState.SAVE_DATA) {
            logger.error("Invalid state! Expected: " + ProcessState.SAVE_DATA + "Got: " + state);
            return false;
        }

        Storage storage = Storage.getInstance();
        storage.addJobListing(jobListing);
        logger.trace("Saved " + jobListing);

        logger.info("Saved: " + storage.getJobListings().size());
        state = ProcessState.FINISHED;
        return true;
    }
}
