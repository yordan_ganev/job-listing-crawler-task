package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.Main;
import com.yng.oop.crawler.consumer.joblisting.JobListing;
import com.yng.oop.crawler.consumer.joblisting.JobListingBuilder;
import com.yng.oop.crawler.fetch.PageFetch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Extractor implementation for job listings from zaplata.bg
 */
public class ZaplataBgDataExtractor implements IJobListingDataExtractor {
    Logger logger = LogManager.getLogger(ZaplataBgDataExtractor.class);
    PageFetch pageFetch;

    /**
     * @param document The HTML document to parse
     * @return The JobListing object containing extracted data, or null if extraction fails
     */
    @Override
    public JobListing parseDocument(Document document) {

        // Title and URL
        Elements tmpElements = document.select(".info > .box > .title > strong");
        String title = tmpElements.get(0).text();

        logger.trace("title: " + title);

        if (title == null || title.isBlank())
            return null;

        JobListingBuilder builder = new JobListingBuilder(document.baseUri(), title);

        // Try to collect optional data
        String firm = "";
        String location = "";
        String salaryMin = "";
        String salaryMax = "";

        tmpElements = document.select(".info > .box > .title > span"); //ok
        if (!tmpElements.isEmpty()) {
            firm = tmpElements.get(0).text();
        }

        tmpElements = document.select(".info .params .location");
        if (!tmpElements.isEmpty()) {
            location = tmpElements.get(0).text();
        }

        tmpElements = document.select(".info .params .salary strong");
        if (tmpElements.size() == 2) {
            salaryMin = tmpElements.get(0).text();
            salaryMax = tmpElements.get(1).text();
        } else if (!tmpElements.isEmpty()) {
            salaryMin = tmpElements.get(0).text();
        }

        logger.trace("firm: " + firm);
        logger.trace("location: " + location);
        logger.trace("salaryMin: " + salaryMin);
        logger.trace("salaryMax: " + salaryMax);

        return builder.firm(firm)
                .location(location)
                .salaryMin(salaryMin)
                .salaryMax(salaryMax)
                .build();
    }
}
