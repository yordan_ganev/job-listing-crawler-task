package com.yng.oop.crawler.consumer;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import com.yng.oop.crawler.consumer.joblisting.extractor.ZaplataBgDataExtractor;
import com.yng.oop.crawler.consumer.process.ProcessJobListing;
import com.yng.oop.crawler.fetch.IPageFetcher;
import com.yng.oop.crawler.fetch.PageFetch;
import com.yng.oop.crawler.storage.Storage;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Manager class for getting job listings urls.
 */
@AllArgsConstructor
public class JobListingCrawManager implements Runnable {
    final Logger logger = LogManager.getLogger(JobListingCrawManager.class);
    private final BlockingQueue<String> queue;

    /**
     * Runs the job listing crawler manager as Runnable
     */
    @Override
    public void run() {
        while (true) {
            try {
                String jobListingURL = queue.poll(15, TimeUnit.SECONDS);
                if (jobListingURL == null) {
                    logger.info("Job Listing Crawler Timed Out");
                    return;
                }

                logger.trace("!Read: " + jobListingURL);
                new ProcessJobListing(jobListingURL).process();
                logger.info("Queue left: " + queue.size());
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
                logger.error("Job Listing Crawler Interrupted");
                return;
            }
        }
    }
}
