package com.yng.oop.crawler.consumer.joblisting;

import lombok.*;

/**
 * Class represents a job listing
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class JobListing {
    /**
     * The URL of the job listing.
     */
    private String URL;

    /**
     * The title of the job listing.
     */
    private String title;

    /**
     * The maximum salary of the job listing.
     */
    private String salaryMax;

    /**
     * The minimum salary of the job listing.
     */
    private String salaryMin;

    /**
     * The location of the job listing.
     */
    private String location;

    /**
     * The firm or business offering the job listing.
     */
    private String firm;
}
