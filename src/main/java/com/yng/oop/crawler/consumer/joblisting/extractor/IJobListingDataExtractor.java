package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.jsoup.nodes.Document;

/**
 * Interface for extracting job listing data from HTML documents
 */
public interface IJobListingDataExtractor {
    /**
     * Parses the specified HTML document and extracts job listing data
     *
     * @param doc The HTML document to parse as jsoup.nodes.Document
     * @return The JobListing object containing extracted data, or null if extraction fails
     */
    JobListing parseDocument(Document doc);
}
