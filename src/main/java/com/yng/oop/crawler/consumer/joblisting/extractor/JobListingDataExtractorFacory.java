package com.yng.oop.crawler.consumer.joblisting.extractor;

/**
 * Factory class for creating instances of IJobListingDataExtractor based on URL
 */
public class JobListingDataExtractorFacory implements IJobListingDataExtractorFactory {
    /**
     * Creates an instance of IJobListingDataExtractor based on the provided URL
     * Implemented for jobs.bg, rabota.bg, zaplata.bg
     *
     * @param url The URL for which the data extractor needs to be created
     * @return An instance of IJobListingDataExtractor suitable for the given URL, or null if no suitable extractor is found
     */
    @Override
    public IJobListingDataExtractor createExtractor(String url) {
        if (url.contains("jobs.bg")) {
            return new JobsBgDataExtractor();
        } else if (url.contains("rabota.bg")) {
            return new RabotaBgDataExtractor();
        } else if (url.contains("zaplata.bg")) {
            return new ZaplataBgDataExtractor();
        }

        return null;
    }
}
