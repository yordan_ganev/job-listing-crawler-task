package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import com.yng.oop.crawler.consumer.joblisting.JobListingBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Extractor implementation for job listings from rabota.bg
 */
public class RabotaBgDataExtractor implements IJobListingDataExtractor {
    Logger logger = LogManager.getLogger(RabotaBgDataExtractor.class);

    /**
     * @param doc The HTML document to parse
     * @return The JobListing object containing extracted data, or null if extraction fails
     */
    @Override
    public JobListing parseDocument(Document doc) {
        Elements tmpElements = doc.select("#ofr_position");
        String title = tmpElements.get(0).text();

        if (title == null || title.isBlank()) {
            return null;
        }

        //Try to collect optional data
        String firm = "";
        String location = "";

        tmpElements = doc.select("#ofr_content > div.ofr_gray_bold > b");
        if (!tmpElements.isEmpty()) {
            firm = tmpElements.get(0).text();
        }

        tmpElements = doc.select("#ofr_content > div.ofr_gray > span");
        if (!tmpElements.isEmpty()) {
            location = tmpElements.get(0).text();
        }

        return new JobListingBuilder(doc.baseUri(), title)
                .firm(firm)
                .location(location)
                .build();
    }
}
