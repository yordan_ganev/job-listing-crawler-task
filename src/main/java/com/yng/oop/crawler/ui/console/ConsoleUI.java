package com.yng.oop.crawler.ui.console;

import com.yng.oop.crawler.producer.config.ProducerCrawlerConfiguration;
import com.yng.oop.crawler.storage.Storage;
import com.yng.oop.crawler.storage.export.CSVJobListingExporter;
import com.yng.oop.crawler.storage.export.IJobListingExporter;
import com.yng.oop.crawler.storage.export.JsonJobListingExporter;
import com.yng.oop.crawler.ui.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Console UI for Job Listing Crawler
 */
public class ConsoleUI extends Controller {
    Logger logger = LogManager.getLogger(ConsoleUI.class);
    final static String defaultMenu[] = {"Exit", "Default run", "Start with current setup", "Setup output file name", "Setup extractor threads count"};
    final static String sites[] = {"https://www.jobs.bg", "https://www.rabota.bg/", "https://www.zaplata.bg"};

    /**
     * @brief Constructor for ConsoleUI
     */
    public ConsoleUI() {
        super(Arrays.stream(sites).toList(), 10);
    }

    /**
     * @brief Start crawler executor
     */
    @Override
    public void execute() {
        super.execute();
        while (!executor.isReady()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
                logger.error("Thread delay interrupted");
            }
        }
        exportStorage();
    }


    /**
     * @param menu List of Menu Options String
     * @return Selected input
     * @retval -1 Invalid input
     */
    int selectMenu(List<String> menu) {
        ConsoleClear.clear();

        System.out.println("======JOB=LISTING=CRAWLER======");
        for (int m = 0; m < menu.size(); ++m) {
            System.out.println(m + ". " + menu.get(m));
        }
        System.out.println();
        System.out.println("Current config:");
        if (!getFileName().isBlank()) {
            System.out.println("File name: " + getFileName());
        }
        System.out.println("Extractors: " + getExtractors());
        for (ProducerCrawlerConfiguration cfg :
                producerConfigs) {
            System.out.println(cfg);
        }

        String input = getInput("Select menu option");
        int selected;
        try {
            selected = Integer.parseInt(input);
        } catch (Exception e) {
            logger.error("Invalid input");
            return -1;
        }
        return selected;
    }

    /**
     * @param message Displayed message for prompting user input (has " :" added at the end)
     * @return User input string
     * @note not validated
     */
    String getInput(String message) {
        System.out.print(message + " :");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    /**
     * @brief Starts the console UI
     */
    public void start() {
        // Setup menu
        List<String> menu = new ArrayList<>();

        for (String m :
                defaultMenu) {
            menu.add(m);
        }

        for (String url :
                urls) {
            menu.add("Setup " + url);
        }

        // Console ui
        int select;
        do {
            select = selectMenu(menu);

            switch (select) {
                case -1:
                case 0:
                    break;
                case 1:
                    executeDefault();
                    select = 0;
                    break;
                case 2:
                    logger.trace(producerConfigs.size());

                    execute();
                    select = 0;
                    break;
                case 3:
                    ConsoleClear.clear();
                    setFileName(getInput("Please enter file name"));
                    break;
                case 4:
                    try {
                        int extractors = Integer.parseInt(getInput("Enter number of extractors for consumer"));
                        setExtractors(extractors);
                    } catch (NumberFormatException e) {
                        logger.error(e.getMessage());
                        getInput("Confirm");
                    }
                    break;
                default:
                    int item = select - 5;
                    int crawlers;
                    int depth;

                    try {
                        crawlers = Integer.parseInt(getInput("Enter number of crawlers for " + urls.get(item)));
                        depth = Integer.parseInt(getInput("Enter depth of search for " + urls.get(item)));
                        setup(urls.get(item), crawlers, depth);
                    } catch (NumberFormatException e) {
                        logger.error("Invalid number entered");
                    } catch (IllegalArgumentException e) {
                        logger.error(e.getMessage());
                    } catch (ArrayIndexOutOfBoundsException e) {
                        logger.error("Selected " + select + " is not a menu option! Please select valid menu options only!");
                    } finally {
                        getInput("Confirm (press enter..)");
                    }
                    break;
            }
        } while (select != 0);
    }

    /**
     * @brief Saves Extracted Data from Storage to configured (.csv or .json) or default(.json) file
     */
    void exportStorage() {
        String fileName = getFileName();
        IJobListingExporter exporter;
        File file;
        FileOutputStream outputStream;


        // prepare file, output stream and exporter
        if (fileName.isBlank()) {
            Date now = new Date();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
            fileName = "jobs_" + LocalDateTime.now().format(formatter) + ".json";
        }

        if (fileName.endsWith(".csv")) {
            file = new File(fileName);
            try {
                outputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                logger.error(e.getMessage());
                logger.error("Couldn't finish job listings export");
                return;
            }
            exporter = new CSVJobListingExporter(outputStream);
        } else {
            if (!fileName.endsWith(".json"))
                fileName += ".json";
            file = new File(fileName);
            try {
                outputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                logger.error(e.getMessage());
                logger.error("Couldn't finish job listings export");
                return;
            }
            exporter = new JsonJobListingExporter(outputStream);
        }

        logger.info("Exporting " + Storage.getInstance().getJobListings().size() + "Job Listings Info");

        exporter.export(Storage.getInstance().getJobListings());

        logger.trace(fileName);
    }
}
