package com.yng.oop.crawler.ui;

import com.yng.oop.crawler.CrawlerExecutor;
import com.yng.oop.crawler.producer.config.ProducerCrawlerConfiguration;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller class for Job Listing Crawler UI
 */
public class Controller {
    Logger logger = LogManager.getLogger(Controller.class);
    protected final List<String> urls;
    protected List<ProducerCrawlerConfiguration> producerConfigs;
    protected CrawlerExecutor executor;
    @Getter
    @Setter
    private int extractors;
    @Getter
    @Setter
    private String fileName = new String();


    /**
     * @param urls       List of strings as supported urls of sites to be crawled
     * @param extractors Number of threads extracting job data
     */
    public Controller(List<String> urls, int extractors) {
        this.urls = urls;
        this.extractors = extractors;
        producerConfigs = new ArrayList<>(urls.size());
    }

    /**
     * Start crawling with current configuration
     */
    public void execute() {
        executor = new CrawlerExecutor(producerConfigs, extractors);
        executor.exec();
    }

    /**
     * @brief Start crawling with default configuration
     */
    public void executeDefault() {
        producerConfigs = new ArrayList<>();
        ProducerCrawlerConfiguration config = new ProducerCrawlerConfiguration(10, 3, "https://www.zaplata.bg/");
        producerConfigs.add(config);

        extractors = 10;

        execute();
    }

    /**
     * @param url      Url of crawling producer
     * @param crawlers number of crawlers
     * @param depth    depth for crawling
     */
    protected void setup(String url, int crawlers, int depth) {
        producerConfigs.forEach(cfg -> {
            if (cfg.getUrl() == url)
                producerConfigs.remove(cfg);
        });

        ProducerCrawlerConfiguration config = new ProducerCrawlerConfiguration(crawlers, depth, url);
        producerConfigs.add(config);
    }

}
