package com.yng.oop.crawler.ui.console;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Clear console class
 */
public class ConsoleClear {
    static final Logger logger = LogManager.getLogger();

    /**
     * Clear console on any OS
     */
    public final static void clear() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            logger.error("Console clear error");
        }
    }

}
