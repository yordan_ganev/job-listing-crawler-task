package com.yng.oop.crawler.fetch;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * IPageFetcher using Jsoup for fetching web pages
 */
public class JsoupPageFetcher implements IPageFetcher {
    /**
     * Fetches the web page content from the specified URL using Jsoup library
     *
     * @param url The URL of the web page to fetch
     * @return The Document object representing the fetched web page as jsoup.nodes.Document
     * @throws IOException If occurs while fetching the web page
     */
    @Override
    public Document fetch(String url) throws IOException {
        return Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:122.0) Gecko/20100101 Firefox/122.0")
                .get();
    }
}
