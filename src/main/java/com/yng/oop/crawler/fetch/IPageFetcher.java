package com.yng.oop.crawler.fetch;

import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Interface for fetching web pages.
 */
public interface IPageFetcher {
    /**
     * Fetches the web page content from the specified URL.
     *
     * @param url The URL of the web page to fetch
     * @return The Document object representing the fetched web page as jsoup.nodes.Document
     * @throws IOException error while fetching the web page.
     */
    Document fetch(String url) throws IOException;
}
