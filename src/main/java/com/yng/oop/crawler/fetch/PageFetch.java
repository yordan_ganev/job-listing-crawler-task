package com.yng.oop.crawler.fetch;

import com.yng.oop.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Class for fetching web pages.
 */
public class PageFetch {
    Logger logger = LogManager.getLogger(Main.class);
    private final IPageFetcher pageFetcher;

    /**
     * Constructs a PageFetch with specified page fetcher implementation.
     *
     * @param pageFetcher The page fetcher implementation to use.
     */
    public PageFetch(IPageFetcher pageFetcher) {
        this.pageFetcher = pageFetcher;
    }

    /**
     * @param url The URL of the web page to fetch
     * @return The Document object representing the fetched web page, or null if error occurs
     */
    public Document fetch(String url) {
        try {
            return pageFetcher.fetch(url);
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
