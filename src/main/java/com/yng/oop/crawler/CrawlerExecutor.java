package com.yng.oop.crawler;

import com.yng.oop.crawler.consumer.JobListingCrawManager;
import com.yng.oop.crawler.fetch.JsoupPageFetcher;
import com.yng.oop.crawler.producer.JobListingsSearchCrawler;
import com.yng.oop.crawler.producer.config.ProducerCrawlerConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Executes multiple web crawlers based on configurations provided.
 * Manages the execution of web crawling tasks for producer and consumer threads.
 */
public class CrawlerExecutor {
    Logger logger = LogManager.getLogger(CrawlerExecutor.class);
    List<ProducerCrawlerConfiguration> configList;
    int extractors;
    ExecutorService executor;

    private static final int MAX_EXTRACTORS = 50;

    /**
     * @param configList The list of ProducerCrawlerConfiguration objects specifying the configurations for each crawler.
     * @param extractors number of extractor threads to be used for processing job listings.
     */
    public CrawlerExecutor(List<ProducerCrawlerConfiguration> configList, int extractors) {
        if (extractors <= 0 || extractors > MAX_EXTRACTORS) {
            logger.error("Extractors of" + extractors + "not in rage [1;" + MAX_EXTRACTORS + "]");
            throw new InvalidParameterException();
        }
        if (configList == null) {
            logger.error("ProducerCrawlerConfiguration list is null");
            throw new InvalidParameterException();
        }

        this.configList = configList;
        this.extractors = extractors;
    }

    /**
     * Executes the web crawlers based on the provided configurations.
     */
    public void exec() {
        BlockingQueue<String> queue = new LinkedBlockingQueue<>();

        int threadsCount = extractors;
        for (ProducerCrawlerConfiguration cfg :
                configList) {
            threadsCount += cfg.getCrawlers();
        }

        executor = Executors.newFixedThreadPool(threadsCount);

        for (ProducerCrawlerConfiguration cfg :
                configList) {
            for (int i = 0; i < cfg.getCrawlers(); ++i) {
                executor.submit(new JobListingsSearchCrawler(queue, new JsoupPageFetcher(), cfg));
            }
        }

        for (int i = 0; i < extractors; ++i) {
            executor.submit(new JobListingCrawManager(queue));
        }

        executor.shutdown();
    }

    /**
     * Checks if the executor has terminated.
     *
     * @return True if the executor is not null and has terminated, false otherwise.
     */
    public boolean isReady() {
        return (executor != null && executor.isTerminated());
    }
}
