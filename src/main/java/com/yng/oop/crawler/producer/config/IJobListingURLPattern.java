package com.yng.oop.crawler.producer.config;

import java.util.regex.Pattern;

/**
 * Interface for providing URL patterns for job listing websites.
 */
public interface IJobListingURLPattern {
    /**
     * @param url The URL of the website.
     * @return The pattern for the website.
     */
    Pattern getPattern(String url);
}
