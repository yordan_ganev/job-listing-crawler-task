package com.yng.oop.crawler.producer;

import com.yng.oop.crawler.fetch.IPageFetcher;
import com.yng.oop.crawler.fetch.PageFetch;
import com.yng.oop.crawler.producer.config.ProducerCrawlerConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.concurrent.BlockingQueue;

/**
 * Thread Job Listing Search Crawler by passing threw page links, Producer of urls of job listing
 */
public class JobListingsSearchCrawler implements Runnable {
    Logger logger = LogManager.getLogger(JobListingsSearchCrawler.class);
    private final BlockingQueue<String> queue;
    private static volatile HashSet<String> links;
    private final IPageFetcher fetcher;
    private final ProducerCrawlerConfiguration cfg;

    /**
     * @param q       The blocking queue to add discovered job listing URLs to
     * @param fetcher The page fetcher implementation used to fetch web pages
     * @param cfg     The configuration for the crawler
     */
    public JobListingsSearchCrawler(BlockingQueue<String> q, IPageFetcher fetcher, ProducerCrawlerConfiguration cfg) {
        if (links == null)
            links = new HashSet<>();

        this.queue = q;
        this.fetcher = fetcher;
        this.cfg = cfg;
    }

    /**
     * Runs the job listings search crawler from Runnable interface
     */
    @Override
    public void run() {
        links.add(cfg.getUrl());

        depthSearch(cfg.getUrl(), 0);

        logger.info("Search Crawler Ready");
    }

    /**
     * @param url   The URL to start the search from.
     * @param depth The current depth of the search.
     */
    void depthSearch(String url, int depth) {
        if (depth < cfg.getDepth()) {
            Document document = new PageFetch(fetcher).fetch(url);
            Elements linksOnPage = document.select("a[href]");

            depth++;
            for (Element page : linksOnPage) {

                String tmpLink = page.attr("abs:href");

                if (tmpLink.startsWith(cfg.getUrl()) && !(containsLink(tmpLink))) {
                    logger.trace(">> Depth: " + depth + " [" + tmpLink + "]");
                    addLink(tmpLink);

                    if (cfg.getPattern().matcher(tmpLink).matches()) {
                        logger.trace(">> JOB: [" + tmpLink + "]");
                        queue.add(tmpLink);
                    }

                    depthSearch(tmpLink, depth);
                }
            }
        }
    }

    /**
     * Adds a URL to the set of discovered links
     *
     * @param url The URL to add
     */
    synchronized void addLink(String url) {
        links.add(url);
    }

    /**
     * @param url The URL to check for
     * @return True if the URL is already in the set, false otherwise
     * @brief Checks if the set of discovered links contains a specific URL
     */
    synchronized boolean containsLink(String url) {
        return links.contains(url);
    }
}
