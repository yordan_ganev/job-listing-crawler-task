package com.yng.oop.crawler.producer.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Pattern;

/**
 * A singleton class for providing URL patterns for job listing websites
 */
public final class JobListingURLPattern implements IJobListingURLPattern {
    Logger logger = LogManager.getLogger(JobListingURLPattern.class);
    private static volatile JobListingURLPattern instance;
    Pattern jobsbg;
    Pattern rabotabg;
    Pattern zaplatabg;

    /**
     * Private constructor to prevent illegal calling
     */
    private JobListingURLPattern() {
        compilePatterns();
    }

    /**
     * @param url The URL of the website
     * @return The URL pattern for the website
     */
    @Override
    public Pattern getPattern(String url) {
        if (url.contains("jobs.bg")) {
            return jobsbg;
        } else if (url.contains("rabota.bg")) {
            return rabotabg;
        } else if (url.contains("zaplata.bg")) {
            return zaplatabg;
        }

        logger.info("Unable to find propper pattern for" + url);
        return null;
    }

    /**
     * Gets the singleton instance of JobListingURLPattern with double-checked locking
     *
     * @return The singleton instance of JobListingURLPattern
     */
    static public JobListingURLPattern getInstance() {
        JobListingURLPattern result = instance;

        //double-checked locking
        if (result != null) {
            return result;
        }

        synchronized (JobListingURLPattern.class) {
            if (instance == null) {
                instance = new JobListingURLPattern();
            }
            return instance;
        }
    }

    /**
     * Compiles regular expression patterns for each job listing website
     */
    private void compilePatterns() {
        jobsbg = Pattern.compile("https://www.jobs.bg/job/\\d+");
        rabotabg = Pattern.compile("https://www.rabota.bg/[a-z-]+/\\d{4}/\\d{2}/\\d{2}/[a-zA-Z0-9-]+.\\d+");
        zaplatabg = Pattern.compile("https://www.zaplata.bg/[a-z\\-]+/[a-z\\-]+/\\d+/[a-z\\-]+/");
        logger.trace("Compiled Regex Patterns");
    }
}
