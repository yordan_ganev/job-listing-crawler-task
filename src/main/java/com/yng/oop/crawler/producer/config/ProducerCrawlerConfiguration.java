package com.yng.oop.crawler.producer.config;

import lombok.Getter;
import lombok.ToString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.regex.Pattern;

/**
 * Configuration class for the producer search crawler
 */
@Getter
@ToString
public class ProducerCrawlerConfiguration {
    @ToString.Exclude
    private final Logger logger = LogManager.getLogger(ProducerCrawlerConfiguration.class);
    private static final int MAX_CRAWLERS = 25;
    private static final int MAX_DEPTH = 6;

    final int crawlers;
    final int depth;
    final String url;
    final Pattern pattern;

    /**
     * @param crawlers The number of crawler threads to use
     * @param depth    The depth of crawling
     * @param url      The URL to start crawling from
     */
    public ProducerCrawlerConfiguration(int crawlers, int depth, String url) {
        if (depth < 0 || depth > MAX_DEPTH) {
            logger.error("Depth of " + depth + " is not in range of [0;" + MAX_DEPTH + "]");
            throw new IllegalArgumentException();
        }

        if (crawlers <= 0 || crawlers > MAX_CRAWLERS) {
            logger.error("Number of crawlers " + crawlers + " is not in range of [1;" + MAX_CRAWLERS + "]");
            throw new IllegalArgumentException();
        }

        try {
            new URL(url).toURI();
        } catch (Exception e) {
            logger.error("Illegal URL provided");
            logger.error(e.getMessage());
            logger.error(e.getStackTrace().toString());
            throw new IllegalArgumentException();
        }

        this.crawlers = crawlers;
        this.depth = depth;
        this.url = url;
        this.pattern = JobListingURLPattern.getInstance().getPattern(url);
    }
}
