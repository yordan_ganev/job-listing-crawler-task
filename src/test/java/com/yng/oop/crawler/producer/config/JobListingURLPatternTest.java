package com.yng.oop.crawler.producer.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.regex.Pattern;

public class JobListingURLPatternTest {
    Logger logger = LogManager.getLogger(JobListingURLPatternTest.class);

    @ParameterizedTest
    @CsvSource({
            "https://www.jobs.bg/edu/search, false",
            "https://www.jobs.bg/front_job_search.php,false",
            "https://www.jobs.bg/company/311860,false",
            "https://www.jobs.bg/job/7254346,true",
            "https://www.jobs.bg/job/7236107,true",
            "https://www.rabota.bg/employer.php?id=42729, false",
            "https://www.rabota.bg/search.php?page=57,false",
            "https://www.rabota.bg/media/interviews/vuzf-sbu-i-napi-vrachiha-parvite-godishni-nagradi-za-obrazovanie-po-ikonomika-v-srednite-uchilishta-za-2023-g,false",
            "https://www.rabota.bg/marketing/2017/10/02/project-support-administrator-fluent-in-english-for-hewlett-packard-enterprise-account.309301,true",
            "https://www.rabota.bg/it-telekomunikacii/2024/01/24/otc-process-expert.391345,true",
            "https://www.zaplata.bg/bild-km/, false",
            "https://www.zaplata.bg/blog/vazhno-umenie-za-liderstvo/, false",
            "https://www.zaplata.bg/prodavachi-i-pomoshten-personal/sofia/616436/skladovi-rabotnitsi-motopista/, true",
            "https://www.zaplata.bg/prodavachi-i-pomoshten-personal/plovdiv/630854/spetsialist-marketing-i-prodazhbi/, true"
    })
    void testJobListingURLPattern(String URL, boolean expected) {
        IJobListingURLPattern urlPattern = JobListingURLPattern.getInstance();
        Pattern jobListingPattern = urlPattern.getPattern(URL);

        Assertions.assertNotNull(jobListingPattern);

        logger.trace(URL + " " + expected + " " + jobListingPattern.matcher(URL).matches());

        Assertions.assertEquals(expected, jobListingPattern.matcher(URL).matches());
    }

    @Test
    void testJobListingURLPatternSingleton() {
        IJobListingURLPattern urlPattern1 = JobListingURLPattern.getInstance();
        IJobListingURLPattern urlPattern2 = JobListingURLPattern.getInstance();

        Assertions.assertEquals(urlPattern1, urlPattern2);
    }
}
