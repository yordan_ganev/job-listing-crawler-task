package com.yng.oop.crawler.producer.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProducerCrawlerConfigurationTest {
    @Test
    void testProducerCrawlerConfigurationInvCrawlers() {
        int crawlers = 1234;
        int depth = 2;
        String url = "https://www.jobs.bg";

        Assertions.assertThrows(IllegalArgumentException.class, () -> new ProducerCrawlerConfiguration(crawlers, depth, url));
    }

    @Test
    void testProducerCrawlerConfigurationInvDepth() {
        int crawlers = 11;
        int depth = 123;
        String url = "https://www.jobs.bg";

        Assertions.assertThrows(IllegalArgumentException.class, () -> new ProducerCrawlerConfiguration(crawlers, depth, url));
    }

    @Test
    void testProducerCrawlerConfigurationInvURL() {
        int crawlers = 11;
        int depth = 123;
        String url = "jobs.bg";

        Assertions.assertThrows(IllegalArgumentException.class, () -> new ProducerCrawlerConfiguration(crawlers, depth, url));
    }

    @Test
    void testProducerCrawlerConfiguration() {
        int crawlers = 1;
        int depth = 1;
        String url = "https://www.jobs.bg";
        String jobListing = "https://www.jobs.bg/job/1001001";

        ProducerCrawlerConfiguration config = new ProducerCrawlerConfiguration(crawlers, depth, url);

        Assertions.assertTrue(config.getPattern().matcher(jobListing).matches());
    }
}
