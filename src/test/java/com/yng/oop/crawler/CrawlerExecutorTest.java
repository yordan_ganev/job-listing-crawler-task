package com.yng.oop.crawler;

import com.yng.oop.crawler.producer.config.ProducerCrawlerConfiguration;
import com.yng.oop.crawler.storage.Storage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CrawlerExecutorTest {
    Logger logger = LogManager.getLogger(CrawlerExecutorTest.class);

    @Test
    void testCrawlerExecutor() {
        List<ProducerCrawlerConfiguration> configList = new ArrayList<>();
        ProducerCrawlerConfiguration config = new ProducerCrawlerConfiguration(10, 3, "https://www.zaplata.bg/");
        configList.add(config);

        CrawlerExecutor executor = new CrawlerExecutor(configList, 10);
        executor.exec();

        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(2), () -> {
            while (!executor.isReady()) {
                Thread.sleep(100);
            }
        });
    }

    @Test
    void testCrawlerExecutorMultisite() {
        List<ProducerCrawlerConfiguration> configList = new ArrayList<>();
        configList.add(new ProducerCrawlerConfiguration(10, 3, "https://www.zaplata.bg/"));
        configList.add(new ProducerCrawlerConfiguration(10, 3, "https://www.rabota.bg/"));

        CrawlerExecutor executor = new CrawlerExecutor(configList, 20);
        executor.exec();

        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(3), () -> {
            while (!executor.isReady()) {
                Thread.sleep(100);
            }
        });
    }
}
