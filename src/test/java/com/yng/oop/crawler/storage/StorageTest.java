package com.yng.oop.crawler.storage;

import com.yng.oop.crawler.consumer.joblisting.JobListingBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class StorageTest {
    Logger logger = LogManager.getLogger(StorageTest.class);

    @Test
    void testStorageSingleton() {
        Storage expected = Storage.getInstance();
        Storage actual = Storage.getInstance();

        Assertions.assertSame(expected, actual);
    }

    @Test
    void testStorageMultiThreading() {
        int THREADS = 5;
        int JOBS_ADDED = 5;
        int TIMEOUT_SEC = 5;

        ExecutorService executor = Executors.newFixedThreadPool(THREADS);

        int currentStorageSize = Storage.getInstance().getJobListings().size();

        for (int tIdx = 0; tIdx < THREADS; ++tIdx) {
            final int threadIdx = tIdx;
            executor.submit(() -> {
                Storage storage = Storage.getInstance();
                for (int i = 0; i < JOBS_ADDED; ++i) {
                    storage.addJobListing(new JobListingBuilder("URL-" + i, "Title-" + i).build());
                    logger.trace(" Adding Job: " + i);
                }
            });
        }
        executor.shutdown();

        try {
            boolean tasksCompleted = executor.awaitTermination(TIMEOUT_SEC, TimeUnit.SECONDS);
            Assertions.assertTrue(tasksCompleted);
        } catch (InterruptedException e) {
            Assertions.fail();
        }


        Assertions.assertEquals(currentStorageSize + THREADS * JOBS_ADDED, Storage.getInstance().getJobListings().size());
    }

}
