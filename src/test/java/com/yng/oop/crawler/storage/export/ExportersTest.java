package com.yng.oop.crawler.storage.export;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import com.yng.oop.crawler.consumer.joblisting.JobListingBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ExportersTest {
    Logger logger = LogManager.getLogger(ExportersTest.class);
    static List<JobListing> jobListings;
    String outputPath = "src/test/output/";
    String path = "src/test/input/";

    @BeforeAll
    static void initTest() {
        // Add data
        jobListings = new ArrayList<>();

        for (int i = 0; i < 3; ++i) {
            jobListings.add(
                    new JobListingBuilder("URL " + i, "Title " + i)
                            .firm("Firm " + i)
                            .location("Location " + i)
                            .salaryMin("Salary Min " + i)
                            .salaryMax("Salary Max " + i)
                            .build()
            );
        }
    }

    @Test
    void testCSVJobListingExporter() {
        File actualFile = new File(outputPath + "export-test.csv");
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(actualFile);
        } catch (FileNotFoundException e) {
            logger.debug(e.getMessage());
            Assertions.fail();
            throw new RuntimeException(e);
        }

        CSVJobListingExporter exporter = new CSVJobListingExporter(fileOutputStream);
        exporter.export(jobListings);

        File expectedFile = new File(path + "export-test.csv");

        try {
            Assertions.assertEquals(-1L, Files.mismatch(expectedFile.toPath(), actualFile.toPath()));
        } catch (IOException e) {
            logger.debug(e.getMessage());
            Assertions.fail();
        }
    }

    @Test
    void testJSONJobListingExporter() {
        File actualFile = new File(outputPath + "export-test.json");
        FileOutputStream fileOutputStream;

        try {
            fileOutputStream = new FileOutputStream(actualFile);
        } catch (FileNotFoundException e) {
            logger.debug(e.getMessage());
            Assertions.fail();
            throw new RuntimeException(e);
        }
        JsonJobListingExporter exporter = new JsonJobListingExporter(fileOutputStream);
        exporter.export(jobListings);

        File expectedFile = new File(path + "export-test.json");

        try {
            Assertions.assertEquals(-1L, Files.mismatch(expectedFile.toPath(), actualFile.toPath()));
        } catch (IOException e) {
            logger.debug(e.getMessage());
            Assertions.fail();
        }
    }

    @Test
    void testJSONJobListingExporterMissingOpt() {
        String url = "URL";
        String title = "Title";

        String firm = "Firm";
        String salaryMin = "1234 лв.";
        String location = "Русе";


        List<JobListing> jobListMissingOpt = new ArrayList<>();
        jobListMissingOpt.add(new JobListingBuilder(url, title).build());
        jobListMissingOpt.add(new JobListingBuilder(url, title).firm(firm).build());
        jobListMissingOpt.add(new JobListingBuilder(url, title).salaryMin(salaryMin).location(location).build());

        File file = new File(outputPath + "export-test.json");
        FileOutputStream fileOutputStream;

        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            logger.debug(e.getMessage());
            Assertions.fail();
            throw new RuntimeException(e);
        }

        // do some magic to create the expected json
        JSONArray root = new JSONArray();
        JSONObject tmp = new JSONObject().put("url", url).put("title", title);
        JSONObject options = new JSONObject();
        tmp.put("options", options);
        root.put(tmp);

        tmp = new JSONObject().put("url", url).put("title", title);
        options = new JSONObject().put("firm", firm);
        tmp.put("options", options);
        root.put(tmp);

        tmp = new JSONObject().put("url", url).put("title", title);
        options = new JSONObject();
        options.put("salaryMin", salaryMin);
        options.put("location", location);
        tmp.put("options", options);
        root.put(tmp);

        String expected = root.toString();

        String actual;
        JsonJobListingExporter exporter = new JsonJobListingExporter(fileOutputStream);
        exporter.export(jobListMissingOpt);

        try {
            actual = Files.readString(file.toPath());
        } catch (IOException e) {
            logger.debug(e.getMessage());
            logger.debug(e.getStackTrace().toString());
            Assertions.fail();
            return;
        }

        logger.trace(expected);
        logger.trace(actual);

        Assertions.assertTrue(expected.equals(actual));

    }
}
