package com.yng.oop.crawler.fetch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;

public class FetchTest {
    Logger logger = LogManager.getLogger(FetchTest.class);

    @Test
    void testPageFetchJsoup() {
        PageFetch fetcher = new PageFetch(new JsoupPageFetcher());
        Document document = fetcher.fetch("https://google.com");

        Elements elements = document.select("html");

        Assertions.assertNotNull(elements);
        logger.trace(elements);
        Assertions.assertFalse(elements.isEmpty());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "https://www.zaplata.bg/proizvodstvo/germania/628177/darvodelets-tischler/",
            "https://www.rabota.bg/it-telekomunikacii/2024/02/14/technical-project-manager.391675/",
            "https://www.jobs.bg/job/7259877"
    })
    void testJobsListingSitesFetch(String URL) {
        PageFetch fetcher = new PageFetch(new JsoupPageFetcher());
        Document document = fetcher.fetch(URL);

        Elements elements = document.select("html");

        Assertions.assertNotNull(elements);
        logger.trace(elements);
        Assertions.assertFalse(elements.isEmpty());
    }

}
