package com.yng.oop.crawler.ui;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;

public class ControllerTest {
    @Test
    void testController() {
        Controller controller = new Controller(new ArrayList<String>(), 1);
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(2), controller::executeDefault);
    }
}
