package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class JobsBgDataExtractorTest {
    Logger logger = LogManager.getLogger(JobsBgDataExtractorTest.class);

    String path = "src/test/input/";

    @Test
    void testJobsBgDataExtractor() {
        String URL = "https://www.jobs.bg/job/7276356";
        String expectedJobTitle = "ОПЕРАТОР НА МАШИНА В ХВП ПРОИЗВОДСТВО";

        File file = new File(path + "jobsbg-test.html");

        logger.trace(file.getAbsolutePath());

        try {
            Document doc = Jsoup.parse(file, "UTF-8", URL);
            JobListing jobListing = new JobsBgDataExtractor().parseDocument(doc);

            logger.trace(jobListing);

            Assertions.assertEquals(expectedJobTitle, jobListing.getTitle());
            Assertions.assertEquals(URL, jobListing.getURL());

        } catch (IOException e) {
            logger.error("Input test file is missing");
            Assertions.fail();
            throw new RuntimeException(e);
        }
    }

    @Test
    void testJobsBgDataExtractorNoPrice() {
        String URL = "https://www.jobs.bg/job/7276356";
        File file = new File(path + "jobsbg-no-price.html");

        try {
            Document doc = Jsoup.parse(file, "UTF-8", URL);
            JobListing jobListing = new JobsBgDataExtractor().parseDocument(doc);
            logger.trace(jobListing);

            Assertions.assertEquals("", jobListing.getSalaryMin());
            Assertions.assertEquals("", jobListing.getSalaryMax());
        } catch (IOException e) {
            logger.error("Input test file is missing");
            Assertions.fail();
            throw new RuntimeException(e);
        }
    }

    @Test
    void testJobsBgDataExtractorSinglePrice() {
        String URL = "https://www.jobs.bg/job/7274415";
        String EXPECTED_MIN_SALARY = "1800 BGN";
        String EXPECTED_MAX_SALARY = "";

        File file = new File(path + "jobsbg-single-price.html");

        try {
            Document doc = Jsoup.parse(file, "UTF-8", URL);
            JobListing jobListing = new JobsBgDataExtractor().parseDocument(doc);
            logger.trace(jobListing);

            Assertions.assertEquals(EXPECTED_MIN_SALARY, jobListing.getSalaryMin());
            Assertions.assertEquals(EXPECTED_MAX_SALARY, jobListing.getSalaryMax());
        } catch (IOException e) {
            logger.error("Input text file is missing");
            Assertions.fail();
            throw new RuntimeException(e);
        }
    }

    @Test
    void testJobsBgDataExtractorPrice() {
        String URL = "https://www.jobs.bg/job/7276356";
        String EXPECTED_MIN_SALARY = "1800 BGN";
        String EXPECTED_MAX_SALARY = "2200 BGN";

        File file = new File(path + "jobsbg-test.html");
        try {
            Document doc = Jsoup.parse(file, "UTF-8", URL);
            JobListing jobListing = new JobsBgDataExtractor().parseDocument(doc);
            logger.trace(jobListing);

            Assertions.assertEquals(EXPECTED_MIN_SALARY, jobListing.getSalaryMin());
            Assertions.assertEquals(EXPECTED_MAX_SALARY, jobListing.getSalaryMax());
        } catch (IOException e) {
            logger.error("Input text file is missing");
            Assertions.fail();
            throw new RuntimeException(e);
        }
    }


}