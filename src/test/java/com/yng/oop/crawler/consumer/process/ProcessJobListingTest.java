package com.yng.oop.crawler.consumer.process;

import com.yng.oop.crawler.storage.Storage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ProcessJobListingTest {
    @ParameterizedTest
    @ValueSource(strings = {
            "https://www.jobs.bg/job/7276200",
            "https://www.zaplata.bg/remont-serviz-poddrazhka/sofia/630700/tehnik-sgradna-poddrazhka/",
            "https://www.rabota.bg/it-telekomunikacii/2024/02/13/senior-net-software-engineer.391653"
    })
    void testProcessJobListing(String url) {
        Storage storage = Storage.getInstance();
        int size = storage.getJobListings().size();

        ProcessJobListing listingProcessor = new ProcessJobListing(url);
        listingProcessor.process();

        Assertions.assertEquals(size + 1, storage.getJobListings().size());
    }
}
