package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class ZaplataBgDataExtractorTest {
    Logger logger = LogManager.getLogger(ZaplataBgDataExtractorTest.class);
    String path = "src/test/input/";

    @Test
    void testZaplataBgDataExtractorTest() {
        String URL = "https://www.zaplata.bg/proizvodstvo/sofia/628520/komplektovach/";
        String expectedTitle = "Комплектовач";

        File file = new File(path + "zaplatabg-test.html");
        try {
            Document doc = Jsoup.parse(file, "UTF-8", URL);
            JobListing jobListing = new ZaplataBgDataExtractor().parseDocument(doc);
            logger.trace(jobListing);

            Assertions.assertEquals(URL, jobListing.getURL());
            Assertions.assertEquals(expectedTitle, jobListing.getTitle());
        } catch (IOException e) {
            logger.error(e.getMessage());
            Assertions.fail();
        }
    }

    @Test
    void testZaplataBgDataExtractorOptInfo() {
        String expectedFirm = "Х12 ЕООД";
        String expectedLocation = "гр.София";
        String expectedSalaryMin = "1600 лв.";
        String expectedSalaryMax = "2600 лв.";
        String URL = "https://www.zaplata.bg/proizvodstvo/sofia/628520/komplektovach/";
        File file = new File(path + "zaplatabg-test.html");
        try {
            Document doc = Jsoup.parse(file, "UTF-8", URL);
            JobListing jobListing = new ZaplataBgDataExtractor().parseDocument(doc);
            logger.trace(jobListing);

            Assertions.assertEquals(expectedFirm, jobListing.getFirm());
            Assertions.assertEquals(expectedLocation, jobListing.getLocation());
            Assertions.assertEquals(expectedSalaryMin, jobListing.getSalaryMin());
            Assertions.assertEquals(expectedSalaryMax, jobListing.getSalaryMax());
        } catch (IOException e) {
            logger.error(e.getMessage());
            Assertions.fail();
        }
    }

}
