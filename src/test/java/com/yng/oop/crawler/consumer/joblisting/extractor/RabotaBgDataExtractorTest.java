package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class RabotaBgDataExtractorTest {
    Logger logger = LogManager.getLogger(RabotaBgDataExtractorTest.class);
    String path = "src/test/input/";

    @Test
    void testRabotaBgDataExtractor() {
        String URL = "https://www.rabota.bg/it-telekomunikacii/2024/02/13/senior-net-software-engineer.391653";
        String expectedTitle = "Senior .NET Software Engineer";
        String expectedFirm = "BULWORK";
        String expectedLocation = "София";

        File file = new File(path + "rabotabg-test.html");
        try {
            Document doc = Jsoup.parse(file, "windows-1251", URL);
            JobListing jobListing = new RabotaBgDataExtractor().parseDocument(doc);
            logger.trace(jobListing);

            Assertions.assertEquals(URL, jobListing.getURL());
            Assertions.assertEquals(expectedTitle, jobListing.getTitle());
            Assertions.assertEquals(expectedFirm, jobListing.getFirm());
            Assertions.assertEquals(expectedLocation, jobListing.getLocation());
        } catch (IOException e) {
            logger.error(e.getMessage());
            Assertions.fail();
        }
    }
}
