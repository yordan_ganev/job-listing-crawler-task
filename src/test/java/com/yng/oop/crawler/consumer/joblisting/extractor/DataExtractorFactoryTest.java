package com.yng.oop.crawler.consumer.joblisting.extractor;

import com.yng.oop.crawler.consumer.joblisting.JobListing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.IOException;

public class DataExtractorFactoryTest {
    Logger logger = LogManager.getLogger(DataExtractorFactoryTest.class);

    final String path = "src/test/input/";

    @ParameterizedTest
    @ValueSource(strings = {
            "https://www.jobs.bg/job/7276356;jobsbg-test.html;UTF-8",
            "https://www.zaplata.bg/proizvodstvo/sofia/628520/komplektovach/;zaplatabg-test.html;UTF-8",
            "https://www.rabota.bg/it-telekomunikacii/2024/02/13/senior-net-software-engineer.391653;rabotabg-test.html;windows-1251"
    })
    void testJobListingDataExtractorFactory(String inputParams) {
        String params[] = inputParams.split(";");
        String URL = params[0];
        String fileName = params[1];
        String encoding = params[2];

        File file = new File(path + fileName);
        try {
            IJobListingDataExtractor dataExtractor = new JobListingDataExtractorFacory().createExtractor(URL);
            Assertions.assertNotNull(dataExtractor);

            Document doc = Jsoup.parse(file, encoding, URL);
            JobListing jobListing = dataExtractor.parseDocument(doc);
            logger.trace(jobListing);
            Assertions.assertFalse(jobListing.getTitle().isBlank() || jobListing.getURL().isBlank());
        } catch (IOException e) {
            logger.error("Input test file is missing");
            Assertions.fail();
        }
    }

    @Test
    void testJobListingDataExtractorFactoryUnknown() {
        IJobListingDataExtractor dataExtractor = new JobListingDataExtractorFacory().createExtractor("https://www.linkedin.com/");
        Assertions.assertNull(dataExtractor);
    }


}
