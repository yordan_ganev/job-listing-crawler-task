# Job Listing Crawler Task

### Task

Да се имплементира уеб кролер, който смъква паралелно обяви за работа. Не е важно колко данни се събират от обявата. Да
има минимум заглавие и URL. Изберете библиотека, която да ви улесни. Кролването трябва да е изцяло в бекенда, т.е.
Библиотеката да не стартира уеб интерфейс. Пример - https://mkyong.com/java/jsoup-basic-web-crawler-example/.

### Specifications

- Кода ви да не нарушава S.O.L.I.D. принципите и да е с high cohesion and lose coupling. (3т.)
- Кролера да може да бъде конфигуриран колко обяви паралелно да смъква. (1т.)
- Кролера да може да бъде конфигуриран от кои сайтове да смъква. Изберете поне 2-3 сайта. Конфигурацията е множество
  измежду тези сайтове. (1т.)
- Събраните данни да бъдат запазвани в паметта. (1т.)
- Запазването да бъде скрито зад API, не е необходимо да използвате база от данни, може да реализирате собсвен in-memory
  storage. (2т.)
- Да се използват поне 3 дизайн шаблона. (2т.)
- Да има поне 65% покритие с юнит тестове. (1т.)
- Кода да е документиран (1т.)
- Class и Sequence диаграми на най-важните части на приложението (1т.)

## Diagrams

### Class Diagram

```plantuml
class Controller 
{
#urls : List<String>
#producerConfigs : List<ProducerCrawlerConfig>
#executor : CrawlerExecutor
-extractors : int
-fileName : String

+Controller(List<String>, int) :
+execute()
+executeDefault()
#setup(String, int, int)
}

class ConsoleUI 
{
-defaultMenu : String[]
-sites : String[]

+ConsoleUI() :
+execute()
+start()
-exportStorage()
-selectMenu(List<String> : int
-getInput(String) : String

}

class CrawlerExecutor {
-configList : List<ProducerCrawlerConfig>
-extractors : int
-executor : ExecutorService
+CrawlerExecutor(List<ProducerCrawlerConfig>, int) : 
+exec()
+isReady() : boolean
}

class ProducerCrawlerConfiguration {
-crawlers : int
-depth : int
-url : string
-pattern : Pattern
+ProducerCrawlerConfig(int,int,String) : 
+@ToString()
}

class JobListingsSearchCrawler {
-queue : BlockingQueue<String>
-{static}links : HashSet<String>
-fetcher : IPageFetcher
-cfg : ProducerCrawlerConfig
+JLSearchCrawler(BlockingQueue<String>, IPageFetcher, ProducerCrawlerConfig) :
+run()
-depthSearch(String,int)
-addLink(String)
-containsLink(String) : boolean
}

class JobListingCrawManager {
-queue: BlockingQueue<String>
+JobListingCrawManager(BlockingQueue<String>) :
+run() 
}

class ProcessJobListing {
-url : String
-document : Document
-jobListing : JobListing
-state : ProcessState
+ProcessJobListing(String) : 
+process()
-getDocument() : boolean
-extractDocumentData() : boolean
-saveData() : boolean
}
' ----------------------------------------------------------------------
class PageFetch {
-pageFetcher : IPageFetcher
+pageFetcher(IPageFetcher) :
+fetch(String) : Document
}

class JsoupPageFetcher {
+fetch(String) : Document
}

class CSVJobListingExporter {
-outputStream : OutputStream
-delimiter : String
+CSVJobListingExporter(OutputStream) : 
@AllArgsConstructor() :
+export(List<JobListing>)
}

class JsonJobListingExporter {
-outputStream : OutputStream
+JsonJobListingExporter(OutputStream) :
+export(List<JobListing>)
-{static}getJsonOptions(JobListing) : JSONObject 
}

class JobsBgDataExtractor {
+parseDocument(Document) : JobListing
}

class RabotaBgDataExtractor {
+parseDocument(Document) : JobListing
}

class ZaplataBgDataExtractor {
+parseDocument(Document) : JobListing
}

class JobListingDataExtractorFactory {
+createExtractor(String) : IJobListingDataExtractor
}
' ----------------------------------------------------------------------
class JobListing {
-URL : String
-title : String
-salaryMax : String
-salaryMin : String
-location : String
-firm : String
+@AllArgsConstructor()
+@NoArgsConstructor()
+@Setter()
+@Getter()
+@ToString()
}

class JobListingBuilder {
-URL : String
-title : String
-salaryMax : String
-salaryMin : String
-location : String
-firm : String
+JobListingBuilder(string,string) :
+salaryMax(String) : self
+salaryMin(String) : self
+location(String) : self
+firm(String) : self
+build() : JobListing
}
' ----------------------------------------------------------------------
class JobListingURLPattern << (S,#FF7700) Singleton >> {
-instance : JobListingURLPattern
-jobsbg : Pattern
-rabotabg : Pattern
-zaplatabg : Pattern
-JobListingURLPattern() :
-compilePatterns()
+getInstance() : JobListingURLPattern
+getPattern(String) : Pattern
}

class Storage << (S,#FF7700) Singleton >> {
-{static}instance : volatile Storage
-jobListings : List<JobListing>
-Storage() : 
+addJobListing(JobListing)
+getJobListings() : List<JobListing>
+getInstance() : Storage
}

' ----------------------------------------------------------------------
interface IJobListingExporter <<interface>> {
+export(List<JobListings>)
}
interface IPageFetcher <<interface>> {
+fetch(String) : Document
}
interface IJobListingDataExtractor <<interface>> {
+parseDocument(Document) : JobListing
}
interface IJobListingDataExtractorFactory <<interface>> {
+createExtractor(String) : IJobListingDataExtractor
}
' ======================================================================
Controller "1" o-- "M" ProducerCrawlerConfiguration
Controller "1" o-- "1" CrawlerExecutor

ConsoleUI "1" -right-|> "1" Controller

CrawlerExecutor "1" o-- "M" JobListingsSearchCrawler
CrawlerExecutor "1" o-- "M" JobListingCrawManager
CrawlerExecutor "1" o-- "M" ProducerCrawlerConfiguration

JobListingCrawManager "1" o-- "1" JobListingDataExtractorFactory
JobListingCrawManager "1" o-- "1" ProducerCrawlerConfiguration

JobListingsSearchCrawler "1" o-- IPageFetcher
JobListingsSearchCrawler "1" o-- ProducerCrawlerConfiguration

ProducerCrawlerConfiguration "1" -- "1" JobListingURLPattern

Storage "1" *-- "M" JobListing
ProcessJobListing "1" *-- "1" JobListingBuilder
ProcessJobListing "1" o-- "1" PageFetch

JobListingBuilder "1" o-- "1" JobListing


ProcessJobListing "M" -- "1" Storage
ConsoleUI "1" -- "1" Storage

' ----------------------------------------------------------------------
JsoupPageFetcher .up.> IPageFetcher
JobsBgDataExtractor ..> IJobListingDataExtractor
RabotaBgDataExtractor ..> IJobListingDataExtractor
ZaplataBgDataExtractor ..> IJobListingDataExtractor
JobListingDataExtractorFactory ..> IJobListingDataExtractorFactory

CSVJobListingExporter ..> IJobListingExporter
JsonJobListingExporter ..> IJobListingExporter

IJobListingDataExtractor "1" --o "1" ProcessJobListing  
IJobListingDataExtractor "1" --o "1" JobListingDataExtractorFactory

IJobListingExporter "1" -- "1" ConsoleUI
IPageFetcher "1" --* "1" PageFetch
' ----------------------------------------------------------------------
/'interface Runnable << interface >> { 
+run()
}
JobListingsSearchCrawler ..> Runnable
JobListingCrawManager ..> Runnable
'/
/'
interface IJobListingURLPattern <<interface>> {
+getPattern(String) : Pattern
}
'/
/'
enum ProcessState {
GET_DOCUMENT,
EXTRACT_DATA,
SAVE_DATA,
FINISHED
}
'/
```

### Sequence Diagrams

- #### Producer

```plantuml
actor CrawlerExecutor as exe
actor JobListingSearchCrawler as crawler
actor PageFetch as fetch
actor JobListingURLPattern as matcher
queue BlockingQueue as q

activate exe
activate q
 
exe -> crawler : Run (Config, Fetcher)
activate crawler
crawler -> fetch : URL
activate fetch
fetch --> crawler : Document
deactivate fetch

== For each link in given URL and depth ==
crawler -> crawler : depthSearch()
activate crawler
crawler -> fetch : a:href link
activate fetch
fetch --> crawler : Document
deactivate fetch
crawler -> matcher : [for:all a] match(a:href link)
activate matcher
matcher --> crawler : match result
deactivate matcher
crawler -> q : [match result = true] add(URL)
deactivate crawler

== Ready ==
crawler --> exe
deactivate crawler
```

- #### Consumer

```plantuml
!pragma teoz true

actor CrawlerExecutor as exe
queue BlockingQueue as q
actor JobListingCrawManager as crawler
actor ProcessJobListing as process
actor PageFetch as fetch
actor DataExtractor as parser
database Storage as db

activate exe
activate q
 
exe -> crawler : Run
activate crawler

== Repetition ==

crawler -> q : PollData() 
q --> crawler : JobListing URL
crawler -> process : process(URL)
activate process

process -> fetch : fetch(URL)
activate fetch
fetch --> process : Document
deactivate fetch

process -> parser : parse(Document)
activate parser
parser --> process : JobListing
deactivate parser

process -> db : JobListing
activate db
db --> process
deactivate db
deactivate process

== Time out ==
{start} crawler -> q : PollData()
{end} crawler -> crawler : \n\nTimeout
{start} <-> {end} : 15 sec no data
crawler --> exe
deactivate crawler
```
